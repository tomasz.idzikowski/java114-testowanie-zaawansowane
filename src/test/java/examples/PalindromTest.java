package examples;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

class PalindromTest {

    //@ValueSource
    @ParameterizedTest(name = "Checking if {0} is palindrome")
    @ValueSource(strings = {"ALA", "AGA", "KAJAK"})
    void isPalindrome(String input) {
        boolean actual = Palindrom.isPalindrome(input);
        System.out.println(actual);
        assertEquals(actual, true);
        assertThat(actual).isTrue();
    }

    //@MethodSource
    @ParameterizedTest(name = "Checking if {0} is palindrome")
    @MethodSource("textProvider")
    void isPalindrom(String input) {
        boolean actual = Palindrom.isPalindrome(input);
        assertEquals(actual, true);
        assertThat(actual).isTrue();
    }

    static Stream<String> textProvider() {
        return Stream.of("AGA", "ALA", "KAJAK");
    }
}