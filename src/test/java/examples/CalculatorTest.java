package examples;


import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertThrows;

class CalculatorTest {

    //pojedyczny test
    @Test
    @DisplayName("Adding two numbers")
    void shouldAddTwoNumbers() {
        int a = 5;
        int b = 6;
        int expected = 11;
        double actual = Calculator.add(a, b);
        assertThat(actual).isEqualTo(expected);
    }

    //testowanie wyjątków
    @Test
    @DisplayName("Division by zero")
    void shouldThrowsException() {
        int a = 5;
        int b = 0;
        assertThrows(ArithmeticException.class,
                () -> Calculator.divide(a, b),
                "Division by zero"); //junit 5
        assertThatThrownBy(() -> Calculator.divide(a, b))
                .isInstanceOf(ArithmeticException.class)
                .hasMessage("You can not divide by zero!");
    }

    //@MethodSource
    @ParameterizedTest(name = "Checking if {0} + {1} = {2}?")
    @MethodSource("numberProvider")
    void shouldAddTwoNumbers(double firstNumber, double secondNumber, double expectedResult) {
        double actual = Calculator.add(firstNumber, secondNumber);
        assertThat(actual).isEqualTo(expectedResult);
    }

    private static Stream<Arguments> numberProvider() {
        return Stream.of(
                Arguments.of(5.0, 5.0, 10.0),
                Arguments.of(3.0, 2.0, 5.0),
                Arguments.of(-5.0, 5.0, 0.0)
        );
    }

    //@ArgumentSource
    @ParameterizedTest(name = "Checking if {0} + {1} = {2}?")
    @ArgumentsSource(NumberArgumentProvider.class)
    void shouldAddTwoNumbersUsingClass(double firstNumber, double secondNumber, double expectedResult) {
        double actual = Calculator.add(firstNumber, secondNumber);
        assertThat(actual).isEqualTo(expectedResult);
    }

    //@CsvSource
    @ParameterizedTest(name = "Checking if {0} + {1} = {2}?")
    @CsvSource({"5.0, 5.0, 10.0", "3.0, 2.0, 5.0", "-5.0, 5.0, 0.0"})
    void shouldAddTwoNumbersUsingCsv(double firstNumber, double secondNumber, double expectedResult) {
        double actual = Calculator.add(firstNumber, secondNumber);
        assertThat(actual).isEqualTo(expectedResult);
    }

    //@CsvFileSource
    @ParameterizedTest(name = "Checking if {0} + {1} = {2}?")
    @CsvFileSource(resources = "/numbers.csv")
    void shouldAddTwoNumbersUsingCsvFile(double firstNumber, double secondNumber, double expectedResult) {
        double actual = Calculator.add(firstNumber, secondNumber);
        assertThat(actual).isEqualTo(expectedResult);
    }
}