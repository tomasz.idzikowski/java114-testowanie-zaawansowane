package mocks.animal;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AnimalServiceTest {

    @Mock
    private AnimalRepository animalRepository;

    @InjectMocks
    private AnimalService animalService;


    @Test
    void shouldReturnAnimalById() {
        //given
        Animal animal = new Animal(1L, "Pies", "Reksio");
        //when
        when(animalRepository.findById(1L)).thenReturn(Optional.of(animal));
        Animal result = animalService.getById(1L);
        //then
        assertThat(result).isEqualTo(animal);
    }

    @Test
    void shouldReturnAnimalByType() {
        //given
        Animal reksio = new Animal(1L,"dog" , "reksio");
        //when
        when(animalRepository.findByType("dog")).thenReturn(Optional.of(reksio));
        Animal actual = animalService.getByType("dog");
        //then
        assertThat(actual).isEqualTo(reksio);

    }

    @Test
    void shouldCreateAnimal() {
        //given
        Animal animal = new Animal(1L, "Pies", "Reksio");
        AnimalDto animalDto= new AnimalDto("Reksio","Pies");
        //when
        when(animalRepository.add(animalDto)).thenReturn(animal);
        Animal actual = animalService.create("Reksio", "Pies");
        //then
        assertThat(actual).isEqualTo(animal);
    }

    @Test
    void shouldThrowExceptionWhenAnimalDoesNotExists() {
        when(animalRepository.findById(anyLong())).thenReturn(Optional.empty());
        assertThatThrownBy(()->animalService.getById(anyLong()))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Animal does not exist");
    }
}